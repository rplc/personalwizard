## MongoDB

collection: **tvshows**

```
{
    kodiId: '123',
    blacklist: false,
    externalId: '1234',
    kodiScrapeTS: Date.now,
    externalScrapeTS: Date.now,
    kodiData: {
        ...
    },
    externalData: {
        ...
    }
}
```