module.exports = {
    'kodi': '',
    'tmdbAPI': '',
    'mongo': '',
    'darksky': {
        'api': '',
        'latitude': 0,
        'longitude': 0
    },
    'vapid': {
        'publicKey':'',
        'privateKey':''
    },
    'mailer': {
        'smtpConfig': {
            'host': '',
            'port': 465,
            'secure': true,
            'auth': {
                'user': '',
                'pass': ''
            }
        },
        'receiver': ''
    },
    'binaryClock': {
        'host': '',
        'port': ''
    },
    'gasAPI': ''
}