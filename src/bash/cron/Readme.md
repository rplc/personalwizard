# Cron jobs
- periodic folder will be copied to /etc/periodic
- rootCrontabs will be copied to /etc/crontabs/root

## Cron job markup

- **Alpine Linux**: `#!/bin/sh`
- no .sh at the end of the filename
- needs to be executable on the host